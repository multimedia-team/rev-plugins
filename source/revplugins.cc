// ----------------------------------------------------------------------------
//
//  Copyright (C) 2003-2022 Fons Adriaensen <fons@linuxaudio.org>
//    
//  This program is free software; you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation; either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
// ----------------------------------------------------------------------------


#include "reverbs.h"


//-----------------------------------------------------------------------------------
// Common definitions
//-----------------------------------------------------------------------------------


static const char* author = "Fons Adriaensen <fons@linuxaudio.org>";
static const char* license = "GPL3";


static void pconnect (LADSPA_Handle H, unsigned long port, LADSPA_Data *data)
{
    ((LadspaPlugin *)H)->setport (port, data);
}

static void activate (LADSPA_Handle H)
{
    ((LadspaPlugin *)H)->active (true);
}

static void runplugin (LADSPA_Handle H, unsigned long k)
{
    ((LadspaPlugin *)H)->runproc (k, false);
}
/*
  static void runadding (LADSPA_Handle H, unsigned long k)
  {
  ((LadspaPlugin *)H)->runproc (k, true);
  }

  static void setadding (LADSPA_Handle H, LADSPA_Data gain)
  {
  ((LadspaPlugin *)H)->setgain (gain);
  }
*/
static void deactivate (LADSPA_Handle H)
{
    ((LadspaPlugin *)H)->active (false);
}

static void cleanup (LADSPA_Handle H)
{
    delete (LadspaPlugin *) H;
}


//-----------------------------------------------------------------------------------
//  LADSPA dlsym interface
//-----------------------------------------------------------------------------------


#define NMODS 2


static const LADSPA_Descriptor moddescr [NMODS] =
{
    {
        3701,
        "zita-reverb",
        LADSPA_PROPERTY_REALTIME | LADSPA_PROPERTY_HARD_RT_CAPABLE,
        "zita-reverb",
        author,
        license,
        Ladspa_zita_reverb::NPORT,
        Ladspa_zita_reverb::portdescr,
        Ladspa_zita_reverb::portnames,
        Ladspa_zita_reverb::porthints,
        0,
        Ladspa_zita_reverb::create,
        pconnect,
        activate,
        runplugin,
        0,
        0,
        deactivate,
        cleanup
    },
    {
        3702,
        "zita-rev-amb",
        LADSPA_PROPERTY_REALTIME | LADSPA_PROPERTY_HARD_RT_CAPABLE,
        "zita-rev-amb",
        author,
        license,
        Ladspa_zita_reverb_amb::NPORT,
        Ladspa_zita_reverb_amb::portdescr,
        Ladspa_zita_reverb_amb::portnames,
        Ladspa_zita_reverb_amb::porthints,
        0,
        Ladspa_zita_reverb_amb::create,
        pconnect,
        activate,
        runplugin,
        0,
        0,
        deactivate,
        cleanup
    }  
};


extern "C" const LADSPA_Descriptor *ladspa_descriptor (unsigned long i)
{
    if (i >= NMODS) return 0;
    return moddescr + i;
}

//-----------------------------------------------------------------------------------
